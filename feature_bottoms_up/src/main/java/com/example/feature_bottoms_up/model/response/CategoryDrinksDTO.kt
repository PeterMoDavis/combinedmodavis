package com.example.feature_bottoms_up.model.response


import com.google.gson.annotations.SerializedName

data class CategoryDrinksDTO(
    @SerializedName("drinks")
    val drinks: List<Drink>
){
    data class Drink(
        val idDrink: String,
        val strDrink: String,
        val strDrinkThumb: String
    )
}