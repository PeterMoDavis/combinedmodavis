package com.example.feature_bottoms_up.viewmodel

import com.example.feature_bottoms_up.model.response.CategoryDrinksDTO

data class DrinksState(
    val isLoading: Boolean = false,
    val drinks: List<CategoryDrinksDTO.Drink> = emptyList()
)