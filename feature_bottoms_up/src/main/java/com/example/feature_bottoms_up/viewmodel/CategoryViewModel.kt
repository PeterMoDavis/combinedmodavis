package com.example.feature_bottoms_up.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_bottoms_up.model.BottomsUpRepo
import kotlinx.coroutines.launch

class CategoryViewModel : ViewModel() {

    private val repo by lazy { BottomsUpRepo }
    private val _state = MutableLiveData(CategoryState(isLoading = true))
    val state: LiveData<CategoryState> get() = _state

    init {
        viewModelScope.launch {
            val categoryDTO = repo.getCategories()
            _state.value = CategoryState(categories = categoryDTO.categoryItems)
            Log.e("CategoryViewModel", categoryDTO.toString())
        }
    }
}