package com.example.feature_bottoms_up.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_bottoms_up.model.BottomsUpRepo
import kotlinx.coroutines.launch

class DrinksDetailViewModel : ViewModel() {
    private val repo by lazy { BottomsUpRepo }

    private var _state = MutableLiveData<DrinkDetailState>(DrinkDetailState(isLoading = true))
    val state : LiveData<DrinkDetailState> get() = _state

    fun getDrinkDetailState(drinkDetails: String){
        viewModelScope.launch {
            val drinkDetailState = DrinkDetailState(drinkDetails = repo.getDrinkDetails(drinkDetails.toInt()))
            _state.value = drinkDetailState
        }
    }
}