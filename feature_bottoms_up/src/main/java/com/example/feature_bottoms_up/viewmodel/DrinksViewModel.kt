package com.example.feature_bottoms_up.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_bottoms_up.model.BottomsUpRepo
import kotlinx.coroutines.launch

class DrinksViewModel : ViewModel() {
    private val repo by lazy { BottomsUpRepo }

    private var _state = MutableLiveData<DrinksState>(DrinksState(isLoading = true))
    val state : LiveData<DrinksState> get() = _state

    fun getDrinkState(category: String){
        viewModelScope.launch {
            val drinksState = DrinksState(drinks = repo.getCategoryDrinks(category))
            _state.value = drinksState
        }
    }

}