package com.example.feature_bottoms_up.viewmodel

import com.example.feature_bottoms_up.model.response.CategoryDTO

data class CategoryState(
    val isLoading: Boolean = false,
    val categories: List<CategoryDTO.CategoryItem> = emptyList()
)