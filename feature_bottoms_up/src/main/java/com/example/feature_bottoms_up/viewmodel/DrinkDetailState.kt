package com.example.feature_bottoms_up.viewmodel

import com.example.feature_bottoms_up.model.response.DrinkDetailsDTO

data class DrinkDetailState (
    val isLoading: Boolean = false,
    val drinkDetails: List<DrinkDetailsDTO.Drink> = emptyList()
)