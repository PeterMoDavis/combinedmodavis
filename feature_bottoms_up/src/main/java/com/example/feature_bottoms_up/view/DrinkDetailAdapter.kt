package com.example.feature_bottoms_up.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_bottoms_up.databinding.ItemDrinkDetailBinding
import com.example.feature_bottoms_up.model.response.DrinkDetailsDTO

class DrinkDetailAdapter : RecyclerView.Adapter<DrinkDetailAdapter.DrinkDetailViewHolder>(){
    private var drinkDetails = mutableListOf<DrinkDetailsDTO.Drink>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DrinkDetailViewHolder {
        val binding = ItemDrinkDetailBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return DrinkDetailViewHolder(binding)
    }

    override fun onBindViewHolder(drinkDetailsViewHolder: DrinkDetailViewHolder, position: Int) {
        var drinkDetails = drinkDetails[position]
        drinkDetailsViewHolder.loadDrinkDetails(drinkDetails)
    }

    fun addDrinkDetails(drinkDetails: List<DrinkDetailsDTO.Drink>){
        this.drinkDetails = drinkDetails.toMutableList()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return drinkDetails.size
    }

    class DrinkDetailViewHolder(
        private  val binding: ItemDrinkDetailBinding
    ): RecyclerView.ViewHolder(binding.root){
        fun loadDrinkDetails(drinkDetails: DrinkDetailsDTO.Drink){
            binding.tvGlass.text = drinkDetails.strGlass
            binding.ingredient1.text = drinkDetails.strIngredient1
            binding.ingredient2.text = drinkDetails.strIngredient2
            binding.ingredient3.text = drinkDetails.strIngredient3
        }
    }
}