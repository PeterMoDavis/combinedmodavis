package com.example.feature_bottoms_up.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.feature_bottoms_up.R
import com.example.feature_bottoms_up.databinding.FragmentCategoryBinding
import com.example.feature_bottoms_up.viewmodel.CategoryViewModel

class CategoryFragment : Fragment(R.layout.fragment_category) {
    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel by viewModels<CategoryViewModel>()
    private val categoryAdapter by lazy { CategoryAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.categoryList.adapter = categoryAdapter
        categoryViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.categoryList.apply {
               adapter = CategoryAdapter().apply {
                   addCategories(state.categories)
                   addItemClickListener { category : String ->
                        findNavController().navigate(CategoryFragmentDirections.actionCategoryFragmentToDrinksFragment(category))
                   }
               }
            }
//            categoryAdapter.addCategories(state.categories)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}