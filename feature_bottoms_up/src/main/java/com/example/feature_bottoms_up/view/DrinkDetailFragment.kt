package com.example.feature_bottoms_up.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs

import com.example.feature_bottoms_up.R
import com.example.feature_bottoms_up.databinding.FragmentDrinkDetailBinding
import com.example.feature_bottoms_up.viewmodel.DrinksDetailViewModel

class DrinkDetailFragment : Fragment(R.layout.fragment_drink_detail) {
    private var _binding : FragmentDrinkDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DrinkDetailFragmentArgs>()
    private val drinkDetailViewModel by viewModels<DrinksDetailViewModel>()
    private val drinkDetailAdapter by lazy {DrinkDetailAdapter()}


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )=FragmentDrinkDetailBinding.inflate(inflater, container, false).also{
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkDetailViewModel.getDrinkDetailState(args.getId.toString())
        drinkDetailViewModel.state.observe(viewLifecycleOwner){ drinkDetailState ->
            binding.drinkDetailList.adapter = drinkDetailAdapter.apply {
                addDrinkDetails(drinkDetailState.drinkDetails)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}