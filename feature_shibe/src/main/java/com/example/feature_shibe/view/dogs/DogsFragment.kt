package com.example.feature_shibe.view.dogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.feature_shibe.R
import com.example.feature_shibe.adapter.ShibeAdapter
import com.example.feature_shibe.databinding.FragmentDogListBinding
import com.example.feature_shibe.model.ShibeRepo
import com.example.feature_shibe.viewmodel.ShibeViewModel


class DogsFragment : Fragment(R.layout.fragment_dog_list) {
    private var _binding: FragmentDogListBinding? = null
    private val binding get() = _binding!!
    private val repo by lazy { ShibeRepo(requireContext()) }
    private val shibeViewModel by viewModels<ShibeViewModel>() {
        ShibeViewModel.ShibeViewModelFactory(repo)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDogListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    var liked = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shibeViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.rvDogList.apply {
                adapter = ShibeAdapter(state.shibes, repo)
                with(binding) {
                    btnGrid.setOnClickListener() {
                        layoutManager = GridLayoutManager(context, 3)
                    }
                    btnLinear.setOnClickListener() {
                        layoutManager = LinearLayoutManager(context)
                    }
                    btnStaggered.setOnClickListener() {
                        layoutManager = StaggeredGridLayoutManager(2, 1)
                    }

                }

            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}


