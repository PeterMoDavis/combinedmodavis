package com.example.feature_shibe.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.feature_shibe.databinding.ItemDogPicBinding
import com.example.feature_shibe.model.ShibeRepo
import com.example.feature_shibe.model.local.entity.Shibe
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ShibeAdapter(private val shibes: List<Shibe>, private val repo: ShibeRepo) :
    RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = ShibeViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        var url = shibes[position]
        holder.loadShibeImage(url, repo)
    }

    override fun getItemCount(): Int {
        return shibes.size
    }


    class ShibeViewHolder(
        val binding: ItemDogPicBinding

    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadShibeImage(shibe: Shibe, repo: ShibeRepo) {
            binding.ivDogPic.load(shibe.url)

            val color = if (shibe.isLiked) Color.RED else Color.WHITE
            binding.clipHeart.setColorFilter(color)

            binding.ivDogPic.setOnClickListener() {
                shibe.isLiked = !shibe.isLiked
                CoroutineScope(Dispatchers.IO).launch { repo.shibeDao.update(shibe) }
                val color = if (shibe.isLiked) Color.RED else Color.WHITE
                binding.clipHeart.setColorFilter(color)
            }

        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDogPicBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> ShibeViewHolder(binding) }
        }
    }
}