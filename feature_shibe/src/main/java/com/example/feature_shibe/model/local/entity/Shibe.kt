package com.example.feature_shibe.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "shibe_table")
data class Shibe(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val url: String,
    var isLiked: Boolean = false
)